# JCS Extend Folders #
An extremely light-weight WordPress plugin for use in custom WordPress theme
development that allows you to fetch media items from named media folders 
created with the Folders by Premio WordPress plugin.

### Description ###
* JCS Extend Folders
* Version 1.0.1

### Installation ###
* Download the source.
* Copy/Extract the jcs-extend-folders directory to your WordPress plugins directory.
* Activate the plugin.
* Dependencies:
    * Folders by Premio - A WordPress plug-in that allows site owners to organize
      their media items in logically named folders.

### Usage ###
* Get the taxonomy id of the folder you want
* Get the ids of the media files residing in that folder.
* Use those ids in your custom theme.

### Contact Us ###
* DriveJCS <webadmin@jdacsolutions.com>
* https://drivejcs.com

### Change Log ###
* 1.0.1
    * Added random/no random attachment query ability
