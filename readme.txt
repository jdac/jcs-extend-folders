=== JCS Extend Folders ===
Contributors: jdacjohn
Plugin Name: JCS Extend Folders
Plugin URI: https://bitbucket.org/jdac/jcs-extend-folders/
Tags: wp, Premio, folders, media
Author URI: https://drivejcs.com
Author: DriveJCS
Requires at least: 4.7
Tested up to: 5.9.2
Stable tag: 1.0
Version: 1.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Donate link: https://square.link/u/gvIwQGau

A light-weight WordPress plugin for use in custom WordPress theme development that allows
you to fetch media items from named media folders created with the Folders WordPress plugin by Premio.

== Description ==

JCS Extend Folders is a useful plugin for WordPress custom theme developers using the Folders plugin by Premio to
allow site owners to logically organize their media library into named folders.  The JCS Extend Folders plugin uses
the folder taxonomies created by Premio folders to fetch media items from the media library that reside in a given
folder.

The combination of custom post types, advanced custom fields, Folders by Premio, and JCS Extend Folders let's you
build reusable templates and template parts to display only the images associated with a particular post in whatever
format you, as the theme developer, desires without the hassle of having to manage image galleries within the WordPress
content editor - which can be painful when dealing with a large number of images.

= Requirements =
JCS Extend Folders requires a working, activated version of Folders by Premio in order to work properly.

== Frequently Asked Questions ==

= How do I use JCS Extend Folders in my theme to display images in a Premio Folder? =

First, the media folder has to be attached to the post you're working with.  This is best achieved with a custom post type
and an advanced custom field (Taxonomy) which allows you to specify the media folder for the post. Once you have done
this, getting the id of the media folder is a simple matter of using WordPress' get_field() method.  You then use that
id to get an array of attachments from the folder with JCS Extend Folder's get_attachment_ids_for_folder() method.  Once
you've obtained those ids, use the typical WordPress functions to retrieve the attachments and whatever attachment metadata
you require for your theme.

For detailed usage instructions and examples, please view the [README](https://bitbucket.org/jdac/jcs-extend-folders/)

== Screenshots ==
1. Setting up a Premio Folder as an Advanced Custom Field Taxonomy on a custom post type.

== Changelog ==
= 1.0.1 =
* Added ability to specify random or no randomized order on  attachment queries.

= 1.0 =
* Initial version.

== Upgrade Notice ==

= 1.0.1 =
A new version of JCS Extend Folders is available.  Please upgrade now.

